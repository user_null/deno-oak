const dotenv = require("dotenv");
dotenv.config();
import * as WebSocket from "ws";
import * as jwt from "jsonwebtoken";
const mensaje = `<h3>No he podido entenderle, mediante este chat puede consultar</h3>
<ul>
    <li>Orden</li>
    <li>Stock</li>
    <li>Carrito</li>
</ul>`;
const puerto = 8082;
const server = new WebSocket.Server({ port: puerto });
console.log("Escuchando en el puerto: ", puerto);
server.on(
  "connection",
  (socket: {
    on: (arg0: string, arg1: (message: any) => void) => void;
    send: (arg0: string) => void;
  }) => {
    console.log("NEW CONNECTION");
    socket.on("message", (message) => {
      const { token, text } = JSON.parse(message.toString()); ///// HACK HORRIBLE PARA PASAR DE BUFFER A JSON
      /*
          Ni siquiera me caliento en comprobar en la base de datos si el usuario es válido.
          ¿Por qué?
          SI: El token es válido significa que lo emitió el backend
          SI: El token lo emitio el backend el token tiene datos válidos sacados de Mongo
          Así que es un problema menos, un fetch menos, un dolor de cabeza menos.
          Ya tengo suficiente con el Hack de ******* que tuve que hacer porque a WebSockets se le ocurrió mandar un Buffer en vez de Texto
          Horas desperdiciadas en ese hack: 4hr
          Consultas a Stack Overflow: 0
          Dia: Jueves, el Domingo se entrega.
          Dia: Viernes, todavia me hace gracia este mensaje
         */
      jwt.verify(token, String(process.env.JWT_TOKEN_SECRET || "SECRET"), (err: unknown, decoded: any) => {
        if (!err) {
          text === "Orden"
            ? socket.send(
                `Su orden ${
                  decoded.Orden ? "esta :" + decoded.Orden : "no existe"
                }`
              )
            : text === "Stock"
            ? socket.send(
                `El stock de todos los productos es : ${Math.round(
                  Math.random() * 10 + 0.5
                )}`
              )
            : text === "Carrito"
            ? socket.send(
                `Su carrito ${
                  decoded.Carrito ? "es: " + decoded.Carrito : "no existe"
                }`
              )
            : socket.send(mensaje);
        }
      });
    });
  }
);
