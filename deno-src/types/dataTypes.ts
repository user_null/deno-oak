export type ID = string;

export type LoginData = {
  Email: string;
  Password: string;
};

type MessageType = "User" | "Admin";
type CatList = Array<string>;
type PicList = Array<string>;

export type Messages = {
  ID?: string;
  Email: string;
  Message: string;
  Type: MessageType;
  Date: string;
};

export type Products = {
  ID?: string;
  id?: string;
  Nombre: string;
  Descripcion: string;
  Categoria: CatList;
  Precio: string;
  StockDisponible: string;
  Fotos: PicList;
};
export type Users = {
  ID?: string;
  Nombre: string;
  Telefono: `+${number | ""}`;
  Email: `${string}@${"gmail" | "yahoo" | "hotmail"}.com`;
  Password: string;
  Admin: boolean;
  Direccion: {
    Calle: string;
    Altura: string;
    CodigoPostal: string;
    Piso?: string;
    Departamento?: string;
  };
};
export type Order = {
  OrderId: ID;
  UserId: ID;
  Items: ProductOrder;
  Timestamp: Date;
  State: "Generado" | "Pagado" | "Enviando" | "Finalizado";
  Total: number;
};

/* EXAMPLE
const usuario: Users = {
    Name: "Test",
    Telefono: "+53234123",
    Email: "asd@gmail.com",
    Password: "asdasd",
    Admin : false
}
*/

export type Cart = {
  UserId: ID;
  Productos: ProductOrder;
  TimeStamp: Date;
  DirecciónDeEntrega: Adress;
};

type ProductOrder = Array<{ id: ID; amount: number; price: number }>;
export type Adress = {
  Calle: string;
  Altura: string;
  CodigoPostal: string;
  Piso: string;
  Departamento: string;
};
export type ErrorMessage = "500" | "404" | "Unreacheable";
export type Error_ = {
  unreacheable: ErrorMessage;
  _500: ErrorMessage;
  _400: ErrorMessage;
};
export type SucessMessage = "200";
export type Sucess_ = {
  _200: SucessMessage;
};
export type ValidData = Products | Messages;
export type ValidReturn = Products | Messages | ErrorMessage | SucessMessage;
export type MapedData = Map<ID, ValidData>;
