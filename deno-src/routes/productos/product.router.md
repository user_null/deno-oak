Productos
A) GET api/products
◦ no necesita autenticación previa
◦ listara los productos como un array de objetos
◦ cada objeto contendrá toda la información referida al productos
B) GET api/products/{:category}
◦ no necesita autenticación previa
◦ listara los productos como un array de objetos
◦ Si no existe ningún documento con la categoría pedida, devolver el array vacio
◦ cada objeto contendrá toda la información referida al productos
C) POST api/products
◦ necesita autenticacion previa de un usuario con perfiles de administrador
◦ Validara que todos los campos (salvo el campo Fotos, que se podrá subir posteriormente) estén presentes y cumplan con las especificaciones. Crear el atributo Foto de todas formas con el array vacio. Si falla alguna validación, devolver código 400
◦ Devolver un 201 si el guardado en mongo fue exitoso
D) PATCH api/products/{:productId}
◦ necesita autenticacion previa de un usuario con perfiles de administrador
◦ En funcion del campo que reciba, debera validarlo antes de realizar la modificacion. SI falla alguna validacion devolver codigo 400.

E) DELETE api/products/{:productId}
◦ necesita autenticacion previa de un usuario con perfiles de administrador
◦ Si el Id del producto no existe devolver 404
◦ Tras eliminar el producto, devolvera un 200
