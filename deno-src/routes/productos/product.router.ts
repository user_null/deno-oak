import {
  Router,
  Context,
  ProductsCRUD,
  helpers,
  verify,
  JWT_TOKEN_SECRET,
  validateProduct,
} from "../../deps.ts";
const routerProducts: Router = new Router();

routerProducts
  .get("/api/products", async (ctx: Context) => {
   try{
       const response =  await ProductsCRUD.GetAll();
       ctx.response.body = response.Products
       ctx.response.status = response.Status
   }catch(error){
       console.log(error)
       ctx.response.body = "FAILED"
       ctx.response.status = 400
   }
  })

  .get("/api/products/:id", async (ctx: Context) => {
    const { id } = helpers.getQuery(ctx, { mergeParams: true });
      const producto = await ProductsCRUD.Read(id);
      if(producto){
          ctx.response.body = producto
          ctx.response.status = 200
      } else {
          ctx.response.body = []
          ctx.response.status = 404
      }
  })

  .post("/api/products", async (ctx: Context) => {
    const preBody = ctx.request.body({
        contentTypes: {
          json: ["application/json"],
          form: ["multipart", "urlencoded"],
          text: ["text"],
        },
      }),
      body = Object(await preBody.value),
      jwt = await verify(body.token, JWT_TOKEN_SECRET, "HS256"),
      [pass, error] = await validateProduct(body.data);
    if (jwt.Admin === undefined) {
      ctx.response.status = 403;
      ctx.response.body = "No an Admin";
    }
    if (pass) {
      ctx.response.body = await ProductsCRUD.Create(body.data);
      ctx.response.status = 201;
    } else {
      ctx.response.body = error ?? "Not an Admin";
      ctx.response.status = 400;
    }
  })

  .patch("api/products/:id", async (ctx: Context) => {
    const { id } = helpers.getQuery(ctx, { mergeParams: true }),
      preBody = ctx.request.body({
        contentTypes: {
          json: ["application/json"],
          form: ["multipart", "urlencoded"],
          text: ["text"],
        },
      }),
      body = Object(await preBody.value),
      [pass, error] = await validateProduct(body.data);
    if (pass) {
      ctx.response.body = await ProductsCRUD.Update(body.data, id);
      ctx.response.status = 201;
    } else {
      ctx.response.body = error ?? "Not an Admin";
      ctx.response.status = 400;
    }
  })

  .delete("api/products/:id", async (ctx: Context) => {
    const token = ctx.request.headers.get("token") ?? "", // Defaulting to empty strings is always an invalid token, but makes Typescript stop complaining
      jwt = await verify(token, JWT_TOKEN_SECRET, "HS256");
    if (jwt.Admin === undefined) {
      ctx.response.status = 403;
      ctx.response.body = "No an Admin";
      const { id } = helpers.getQuery(ctx, { mergeParams: true });
      ctx.response.status = Number(ProductsCRUD.Delete(id));
    } else {
      ctx.response.body = "Not an Admin";
      ctx.response.status = 400;
    }
  });
export { routerProducts };
