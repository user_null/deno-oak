POST api/user/login
Descripción: Permite obtener un token para acceder a las rutas segurizadas.

Parámetros:
• email
• campo de password

Expectativa de la función:
• Si el usuario existe en la base de datos y la contraseña ingresada es la correcta, responder con un token cuyo tiempo de vida sea configurable por una variable de entorno llamada TOKEN_KEEP_ALIVE
• Si el usuario no existe , devolver una respuesta 401 UnAthorized.
• Si el usuario existe, pero se ingresa una contraseña incorrecta , devolver una respuesta 401 UnAthorized.
• La Secret Key con la cual se genera el token se obtiene de una variable de entorno llamada JWT_SECRET_KEY
