/* A) POST api/user/signup
Descripción: Permite crear un nuevo usuario

Parámetros:
    • nombre completo del cliente, 
    • telefono (1168796652)
    • email
    • campo de password
    • campo de password Repetido
    • admin (true/false)
    • Datos de la direccion de entrega
        ◦ Calle (required)
        ◦ Altura (required)
        ◦ Codigo Postal (required)
        ◦ Piso (opcional)
        ◦ Departamento (opcional)

Validaciones:
    • Si los dos campos de password no coinciden, responder con un 400 Bad Request e indicar en la respuesta cual fue el error
    • Si los formatos de los campos no son validos, responder con un 400 Bad Request e indicar en la respuesta cual fue el error. 
    • El unico campo que no es obligatorio, es el campo de admin. En caso de no enviarse dicho campo, se seteara por default en FALSE.

Expectativa de la función:
    • Guardar usuario en la colección Users en MongoDB
    • Encriptar la contraseña del usuario
    • Crear un documento Carrito para el usuario nuevo con los datos de la direccion de entrega
    • Devolver al usuario un codigo 201 y en la respuesta devolver los datos del usuario */

import {
  Router,
  Context,
  validateSignUp,
  createHash,
  create,
  JWT_TOKEN_SECRET,
  DB_PORT,
} from "../../deps.ts";
const signUpRouter: Router = new Router();
signUpRouter.post("/api/user/signup", async (ctx: Context) => {
  const body = ctx.request.body({
      contentTypes: {
        json: ["application/json"],
        form: ["multipart", "urlencoded"],
        text: ["text"],
      },
    }),
    result = Object(await body.value),
    [pass, error] = await validateSignUp(result);
  !pass && ((ctx.response.status = 400), (ctx.response.body = error));
  if (pass) {
    !(result.Password === result.SecondPassword) && (ctx.response.status = 400);
    result.Admin === undefined && (result.Admin = false);
    // NOTE: Si algo no funciona, recodá que hasheaste la contraseña
    const Hash = createHash("md5");
    Hash.update(result.Password + "SECRET");
    const hashed = Hash.toString();
    result.Password = hashed;
    const jwt = await create(
        { alg: "HS512", typ: "JWT" },
        { document: result, collection: "UserLogin" },
        JWT_TOKEN_SECRET
      ),
      requestBody = JSON.stringify({ token: jwt }),
      request = new Request(`http://node:${DB_PORT}/db/user`, {
        method: "POST",
        body: requestBody,
      });
    const response: Promise<Response> = fetch(request)
      .then((response) => {
        return response;
      })
      .then((response) => {
        return response;
      })
      .catch(() => {
        return response;
      });
    ctx.response.status = (await response).status;
  }
});
export { signUpRouter };
