import {
  Router,
  Context,
  validateLogin,
  createHash,
  create,
  JWT_TOKEN_SECRET,
  DB_PORT,
} from "../../deps.ts";
/* POST api/user/login
   Descripción: Permite obtener un token para acceder a las rutas segurizadas.

   Parámetros:
   • email
   • campo de password

   Expectativa de la función:
   • Si el usuario existe en la base de datos y la contraseña ingresada es la correcta, responder con un token cuyo tiempo de vida sea configurable por una variable de entorno llamada TOKEN_KEEP_ALIVE
   • Si el usuario no existe , devolver una respuesta 401 UnAthorized.
   • Si el usuario existe, pero se ingresa una contraseña incorrecta , devolver una respuesta 401 UnAthorized.
   • La Secret Key con la cual se genera el token se obtiene de una variable de entorno llamada JWT_SECRET_KEY
*/
const loginRouter: Router = new Router();
loginRouter.post("/api/user/login", async (ctx: Context) => {
  const body = ctx.request.body({
      contentTypes: {
        json: ["application/json"],
        form: ["multipart", "urlencoded"],
        text: ["text"],
      },
    }),
    result = Object(await body.value),
    [pass, error] = await validateLogin(result); // Los datos de validacion son pasados por el validador como un tuple [boolean, error (si hay)]
  !pass && (ctx.response.body = error);
  if (pass) {
    // NOTE: Si algo no funciona, recodá que hasheaste la contraseña
    // Encriptación simple de la contraseña
    const Hash = createHash("md5");
    Hash.update(result.Password + JWT_TOKEN_SECRET);
    const hashed = Hash.toString();
    result.Password = hashed;

    const jwt = await create(
        { alg: "HS512", typ: "JWT" },
        { document: result, collection: "UserLogin" },
        JWT_TOKEN_SECRET
      ),
      requestBody = JSON.stringify({ token: jwt }),
      request = new Request(`http://node:${DB_PORT}/db/login`, {
        method: "POST",
        body: requestBody,
      }),
      response = await fetch(request),
      body = await response.text();
    if (body === "Not Found" || body === "Forbidden") {
      ctx.response.status = body === "Not Found" ? 404 : 403;
    } else {
      const jwt = await create(
        { alg: "HS512", typ: "JWT" },
        JSON.parse(body),
        JWT_TOKEN_SECRET
      );
      ctx.response.body = { token: jwt };
    }
  } else {
    ctx.response.body = error;
    ctx.response.status = 400;
  }
});
export { loginRouter };
