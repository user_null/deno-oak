POST api/user/signup
Descripción: Permite crear un nuevo usuario

Parámetros:
• nombre completo del cliente,
• telefono (1168796652)
• email
• campo de password
• campo de password Repetido
• admin (true/false)
• Datos de la direccion de entrega
◦ Calle (required)
◦ Altura (required)
◦ Codigo Postal (required)
◦ Piso (opcional)
◦ Departamento (opcional)

Validaciones:
• Si los dos campos de password no coinciden, responder con un 400 Bad Request e indicar en la respuesta cual fue el error
• Si los formatos de los campos no son validos, responder con un 400 Bad Request e indicar en la respuesta cual fue el error.
• El unico campo que no es obligatorio, es el campo de admin. En caso de no enviarse dicho campo, se seteara por default en FALSE.

Expectativa de la función:
• Guardar usuario en la colección Users en MongoDB
• Encriptar la contraseña del usuarioç
• Crear un documento Carrito para el usuario nuevo con los datos de la direccion de entrega
• Devolver al usuario un codigo 201 y en la respuesta devolver los datos del usuario
