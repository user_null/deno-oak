Carritos
Seguridad:
• Hay que pasarle a todos estos endpoints un token de seguridad en el header bajo la estrategia Bearer Token
A) GET api/cart
◦ listara el carrito del usuario

B) POST api/cart/add
◦ Se pasará como body el id del producto que se desea agregar y la cantidad
◦ Si el id del producto no existe, se devolverá un error 400
◦ Si la cantidad no es valida, se devolverá un error 400
◦ Se devolverá como respuesta el carrito completo con el nuevo producto agregado

Extra:
• Si se desea agregar una cantidad mayor al stock que posee el producto, devolver un error 400
• Una vez agregado el item al carrito, decrementar el stock del producto por la cantidad solicitada

      B) POST api/cart/delete
        ◦ Se pasará como body el id del producto que se desea borrar y la cantidad
        ◦ Si el carrito del usuario no existe, devolver un error 400
        ◦ Si el id del producto no existe, se devolverá un error 400
        ◦ Si la cantidad no es valida, se devolverá un error 400
        ◦ Si se ingresa una cantidad mayor a la que actualmente existe de ese producto en el carrito, se devolverá un error 400
        ◦ Se devolverá como respuesta el carrito completo con el nuevo producto agregado

Extra:

    • Una vez eliminado el item al carrito (parcialmente o totalmente), incrementar el stock del producto por la cantidad solicitada
