/*
Carritos
Seguridad:
• Hay que pasarle a todos estos endpoints un token de seguridad en el header bajo la estrategia Bearer Token
A) GET api/cart
◦ listara el carrito del usuario

B) POST api/cart/add
◦ Se pasará como body el id del producto que se desea agregar y la cantidad
◦ Si el id del producto no existe, se devolverá un error 400
◦ Si la cantidad no es valida, se devolverá un error 400
◦ Se devolverá como respuesta el carrito completo con el nuevo producto agregado
*/
import { Router, Context, CartCR } from "../../deps.ts";
const routerCart: Router = new Router();
routerCart
  .get("/api/cart", async (ctx: Context) => {
    const token = ctx.request.headers.get("token") ?? "",
      preBody = ctx.request.body({
        contentTypes: {
          json: ["application/json"],
          form: ["multipart", "urlencoded"],
          text: ["text"],
        },
      }),
      body = Object(await preBody.value);
    if (token) {
      ctx.response.body = CartCR.Read(body.id);
    } else {
      ctx.response.status = 404;
    }
  })
  .post("/api/cart", async (ctx: Context) => {
    const token = ctx.request.headers.get("token") ?? "",
      preBody = ctx.request.body({
        contentTypes: {
          json: ["application/json"],
          form: ["multipart", "urlencoded"],
          text: ["text"],
        },
      }),
      body = Object(await preBody.value);
    ctx.response.body = CartCR.Create(body.id, token);
    ctx.response.status = 200;
  });
export { routerCart };
