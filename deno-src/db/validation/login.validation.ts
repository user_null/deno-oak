import { isString, required, validate, isEmail } from "../../deps.ts";
import { LoginData } from "../../types/dataTypes.ts";
export const validateLogin = async (x: LoginData) => {
  return await validate(x, {
    Email: [isEmail, required],
    Password: [isString, required],
  });
};
