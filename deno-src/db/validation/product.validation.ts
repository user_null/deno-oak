import { Products } from "../../types/dataTypes.ts";
import { isString, required, validate } from "../../deps.ts";
import { isNumber } from "https://deno.land/x/validasaur@v0.15.0/mod.ts";

export const validateProduct = async (x: Products) => {
  return await validate(x, {
    Nombre: [isString, required],
    Descripcion: [isString, required],
    Categoria: [isString, required],
    Precio: [isNumber, required],
    StockDisponible: [isNumber, required],
    Fotos: [isString, required],
  });
};
