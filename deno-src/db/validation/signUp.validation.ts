import { Users } from "../../types/dataTypes.ts";
import { isString, required, validate } from "../../deps.ts";
import { isEmail } from "../../deps.ts";
import { isBool } from "../../deps.ts";

export const validateSignUp = async (x: Users) => {
  const [pass, error] = await validate(x, {
    Nombre: [isString, required],
    Telefono: [isString, required],
    Email: [isEmail, required],
    Password: [isString, required],
    Admin: [isBool, required],
    Direccion: [required],
  });
  const [passDir, errorDir] = await validate(x.Direccion ?? {}, {
    Calle: [isString, required],
    Altura: [isString, required],
    CodigoPostal: [isString, required],
    Piso: [isString],
    Departamento: [isString],
  });
  return [pass && passDir, error || errorDir];
};
