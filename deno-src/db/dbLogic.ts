import {
  Products,
  persistency,
  create,
  JWT_TOKEN_SECRET,
  DB_PORT,
} from "../deps.ts";
export { ProductsCRUD, CartCR };
const ProductsCRUD = {
    Create: (x: Products) => createMongoProduct(x),
    Read: (x: ID) => readMongo(x),
    Update: (x: Products, y: ID) => updateMongo(x, y),
    Delete: (x: ID) => deleteMongo(x),
    GetAll: () => getAllMongo(),
  },
  CartCR = {
    Create: (x: ID, user: string) => modifyCart(x, user),
    Read: (x: ID) => readUserCart(x),
  },
  // CREATE A NEW PRODUCT
  createMongoProduct = async (x: Products) => {
    const jwt = await create(
        { alg: "HS512", typ: "JWT" },
        { document: x, collection: "Product" },
        JWT_TOKEN_SECRET
      ),
      requestBody = JSON.stringify({ token: jwt }),
      request = new Request(`http://node:${DB_PORT}/db/product`, {
        method: "POST",
        body: requestBody,
      }),
      response: Promise<Response> = fetch(request)
        .then((response) => response)
        .then((response) => response);
    return response;
  },
  // REQUEST UPDATE TO MONGO DB
  updateMongo = async (x: Products, ID: ID) => {
    const jwt = await create(
        { alg: "HS512", typ: "JWT" },
        {
          document: {
            _id: ID,
            ...x,
          },
          collection: "Product",
        },
        JWT_TOKEN_SECRET
      ),
      requestBody = JSON.stringify({ token: jwt }),
      request = new Request(`http://node:${DB_PORT}/db/product`, {
        method: "PATCH",
        body: requestBody,
      }),
      response: Promise<Response> = fetch(request)
        .then((response) => response)
        .then((response) => response);
    return response;
  },
  // SEARCH FOR A SINGLE PRODUCT
  readMongo = async (x: ID) => {
    const request = new Request(`http://node:${DB_PORT}/db/product/?id=${x}`, {
        method: "GET",
      }),
      response = await fetch(request),
      body = await response.json();
    return body;
  },
  // GET ALL PRODUCTS FROM DATABASE
  getAllMongo = async () => {
    const request = new Request(`http://node:${DB_PORT}/db/product/`, {
        method: "GET",
      }),
      response = await fetch(request),
      body = response.status !== 404 ? { Products: await response.json(), Status : response.status } : {Products: [], Status: response.status}
      return body;
  },
  // DELETE A GIVEN PRODUCT FROM MONGO
  deleteMongo = async (x: ID) => {
    const jwt = await create(
        { alg: "HS512", typ: "JWT" },
        {},
        JWT_TOKEN_SECRET
      ),
      request = new Request(`http://node:${DB_PORT}/db/product/?id=${x}`, {
        method: "DELETE",
        headers: { token: jwt },
      }),
      response = await fetch(request),
      body = await response.json();
    return body;
  },
  // MODIFY USER CART
  modifyCart = async (ID: ID, user: string) => {
    const allProducts = await getAllMongo(),
      product = allProducts.Products.filter((x: { id: string }) => x.id === ID)[0],
      jwt = await create(
        { alg: "HS512", typ: "JWT" },
        {
          document: { Product: product, UsuarioToken: user },
          collection: "UserCart",
        },
        JWT_TOKEN_SECRET
      ),
      requestBody = JSON.stringify({ token: jwt }),
      request = new Request(`http://node:${DB_PORT}/db/cart`, {
        method: "PUT",
        body: requestBody,
      }),
      response = await fetch(request),
      body = await response.json();
    return body;
  },
  // READ CART FROM USER
  readUserCart = async (x: ID) => {
    const jwt = await create(
        { alg: "HS512", typ: "JWT" },
        {},
        JWT_TOKEN_SECRET
      ),
      request = new Request(`http://node:${DB_PORT}/db/cart/?id=${x}`, {
        method: "GET",
        headers: { token: jwt },
      }),
      response = await fetch(request),
      body = await response.json();
    return body;
  };

type ID = string;
