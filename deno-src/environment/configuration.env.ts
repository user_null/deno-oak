import "https://deno.land/x/dotenv/load.ts";
export {
  port,
  DB_PORT,
  URI,
  persistency,
  JWT_TOKEN_SECRET,
  JWT_REFRESH_TOKEN_EXP,
  JWT_ACCESS_TOKEN_EXP,
};
const port: number = Number(Deno.env.get("PORT")) ?? 8080,
  DB_PORT: number = Number(Deno.env.get("DB_PORT")) ?? 3000,
  URI: string = Deno.env.get("URI") ?? "",
  persistency: string = Deno.env.get("PERSISTENCY") ?? "MONGO",
  JWT_TOKEN_SECRET: string = Deno.env.get("JWT_TOKEN_SECRET") ?? "SECRET",
  JWT_ACCESS_TOKEN_EXP: string =
    Deno.env.get("JWT_ACCESS_TOKEN_EXP") ?? String(Date.now()),
  JWT_REFRESH_TOKEN_EXP: string =
    Deno.env.get("JWT_REFRESH_TOKEN_EXP") ?? String(Date.now());
