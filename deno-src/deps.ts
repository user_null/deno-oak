// // EXTERNAL DEPENDENCIES
// OAK
// DOCS: https://oakserver.github.io/oak/
export {
  Application,
  Router,
  helpers,
  Context,
  send,
} from "https://deno.land/x/oak/mod.ts";
export type { Middleware } from "https://deno.land/x/oak/mod.ts";
// Validasaur
// DOCS: https://github.com/emsifa/validasaur
export {
  isString,
  required,
  validate,
  isEmail,
  isBool,
} from "https://deno.land/x/validasaur/mod.ts";
// DJWT
// DOCS: https://deno.land/x/djwt@v2.2
export { create, verify } from "https://deno.land/x/djwt/mod.ts";
export type { Header } from "https://deno.land/x/djwt/mod.ts";
// STANDARD LIBRARY
export { createHash } from "https://deno.land/std@0.103.0/hash/mod.ts";

// // TYPES
export type {
  Products,
  Sucess_,
  ValidData,
  MapedData,
  Error_,
  ValidReturn,
} from "./types/dataTypes.ts";
// // LOCAL FILES
// ROUTES EXPORTS
export { routerProducts } from "./routes/productos/product.router.ts";
export { loginRouter } from "./routes/usuario/login.router.ts";
export { signUpRouter } from "./routes/usuario/signup.router.ts";
export { routerCart } from "./routes/carrito/cart.router.ts";
// MIDDLEWARES
export { authenticateToken } from "./middlewares/authenticate.middleware.ts";
// Validation LOGIC
export { validateSignUp } from "./db/validation/signUp.validation.ts";
export { validateLogin } from "./db/validation/login.validation.ts";
export { validateProduct } from "./db/validation/product.validation.ts";
// CONFIGURATION
export {
  DB_PORT,
  JWT_TOKEN_SECRET,
  persistency,
  port,
} from "./environment/configuration.env.ts";
// DB LOGIC
export { ProductsCRUD } from "./db/dbLogic.ts";
export { CartCR } from "./db/dbLogic.ts";
