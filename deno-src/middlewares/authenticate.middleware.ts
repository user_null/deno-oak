import { Middleware, Context, verify, JWT_TOKEN_SECRET } from "../deps.ts";
export const authenticateToken: Middleware = async (ctx: Context, next) => {
  if (ctx.request.method === "POST" || ctx.request.method === "PATCH") {
    // Bypassing Auth
    const preBody = ctx.request.body({
        contentTypes: {
          json: ["application/json"],
          form: ["multipart", "urlencoded"],
          text: ["text"],
        },
      }),
      headers = ctx.request.headers,
      body = Object(await preBody.value),
      token = headers.get("token") || body.token;

    try {
      await verify(token, JWT_TOKEN_SECRET, "HS256");
    } catch {
      ctx.throw(400, "Token doesn't look right");
    }
    if (!body.data) {
      ctx.throw(400, "Yeah, but send some data at least!");
    }
  }
  await next();
};
