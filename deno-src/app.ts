import {
  Application,
  send,
  signUpRouter,
  loginRouter,
  authenticateToken,
  routerProducts,
  routerCart,
  port,
} from "./deps.ts";
console.log("Inicializando servicio DENO...");
const app = new Application();
// Este HACK envia un archivo estático, es horrible, pero es el ejemplo de la documentacion
app.use(async (context, next) => {
  if (context.request.url.pathname === "/") {
    await send(context, context.request.url.pathname, {
      root: `${Deno.cwd()}/static`,
      index: "index.html",
    });
  }
  await next();
});

// NOTE: Rutas que NO necesitan un TOKEN
app
  .use(signUpRouter.routes())
  .use(signUpRouter.allowedMethods())
  .use(loginRouter.routes())
  .use(loginRouter.allowedMethods());
// NOTE: Rutas que necesitan un TOKEN
app
  .use(authenticateToken)
  .use(routerProducts.routes())
  .use(routerProducts.allowedMethods())
  .use(routerCart.routes())
  .use(routerCart.allowedMethods());
console.log("Corriendo en puerto:", port);
await app.listen({ port });
