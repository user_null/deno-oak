const dotenv = require("dotenv");
dotenv.config();
import "reflect-metadata";
import * as jwt from "jsonwebtoken";
import * as Koa from "koa";
import * as koaBody from "koa-body";
import {
  createConnection,
  Connection,
  ConnectionOptions,
  getMongoManager,
  getMongoRepository,
} from "typeorm";
import * as Router from "@koa/router";
import { authenticateToken } from "./middlewares/authenticate.middleware";
import { addTime } from "./helpers/tokenDate.helper";
import { Product } from "./entity/Product";
import { User } from "./entity/User";
const connectionInfo: ConnectionOptions = {
  type: "mongodb",
  url: process.env.URI,
  useNewUrlParser: true,
  synchronize: true,
  logging: true,
  entities: ["src/entity/*.*"],
};
createConnection(connectionInfo)
  .then((connection: Connection) => {
    const app = new Koa();
    const router = new Router();
    app
      .use(
        koaBody({
          jsonLimit: "1kb",
        })
      )
      .use(authenticateToken);
    router.post("/db/user", async (ctx: Koa.Context) => {
      const user = new User();
      const { Nombre, Telefono, Email, Password, Admin, Carrito, Direccion } =
        ctx.request.body.document;
      user.Nombre = Nombre;
      user.Telefono = Telefono;
      user.Email = Email;
      user.Password = Password;
      user.Admin = Admin;
      user.Carrito = Carrito;
      user.Direccion = Direccion;
      ctx.response.status = 201;
      ctx.response.body = "created";
      await connection.manager.save(user);
    });
    router.post("/db/product", async (ctx: Koa.Context) => {
      const { Nombre, Descripcion, Categoria, Precio, StockDisponible, Fotos } =
        ctx.request.body.document;
      const product = new Product();
      product.Nombre = Nombre;
      product.Descripcion = Descripcion;
      product.Categoria = Categoria;
      product.Precio = Precio;
      product.StockDisponible = StockDisponible;
      product.Fotos = Fotos;
      ctx.response.status = 201;
      await connection.manager.save(product);
    });
    router.post("/db/login", async (ctx: Koa.Context) => {
      const document = ctx.request.body.document;
      const manager = getMongoManager();
      const data = await manager.findOne(User, { Email: document.Email });
      if (data !== undefined) {
        const carrito = data.Carrito || [];
        const admin = data.Admin || false;
        const userData = {
          id: data.id,
          Nombre: data.Nombre,
          Direccion: data.Direccion,
          Carrito: carrito,
          Admin: admin,
          iat: addTime(undefined),
          exp: addTime(120), // 2 minutos
        };
        if (data.Password === document.Password) {
          ctx.body = JSON.stringify(userData);
        } else {
          ctx.status = 403;
        }
      }
    });

    router.patch("/db/product", async (ctx: Koa.Context) => {
      const document = ctx.request.body.document,
        { _id } = document;
      const products = getMongoRepository(Product);
      const {
        lastErrorObject: { updatedExisting },
        value,
        ok,
      } = await products.findOneAndReplace({ _id: _id }, document);
      if (ok !== 1) {
        ctx.status = 400;
      }
      if (updatedExisting) {
        ctx.body = {
          from: { ...value },
          to: { _id: value._id, ...document },
        };
        ctx.status = 202;
      }
    });
    router.get("/db/product/", async (ctx: Koa.Context) => {
      const { id } = ctx.request.query;
      const manager = getMongoManager();
      const Entity = await manager.find(Product, {});
      const response =
        id === undefined
          ? Entity
          : Entity.filter((x: Product) => String(x.id) === String(id));
      if (response.length >= 1) {
        ctx.body = response;
        ctx.status = 200;
      } else {
        ctx.status = 404;
      }
    });
    router.delete("/db/product", async (ctx: Koa.Context) => {
      const { id } = ctx.request.query;
      const products = getMongoRepository(Product);
      const { ok } = await products.findOneAndDelete({ id: id });
      if (ok !== 1) {
        ctx.status = 400;
      } else {
        ctx.status = 200;
      }
    });
    router.post("/db/cart", async (ctx: Koa.Context) => {
      const document = ctx.request.body.document;
      const { Product, UsuarioToken } = document;
      const Usuario: { id?: string; Carrito: Array<string> } = Object(
        await jwt.verify(UsuarioToken, process.env.JWT_TOKEN_SECRET)
      );
      const userRepo = getMongoRepository(User);
      try {
        const userToUpdate = await userRepo.findOne(Usuario.id);
        Usuario.Carrito.push(Product);
        userToUpdate.Carrito = Usuario.Carrito;
        userRepo.save(userToUpdate);
      } catch {
        ctx.status = 400;
      }
      ctx.status = 201;
    });
    router.get("/db/cart", async (ctx: Koa.Context) => {
      const { id } = ctx.request.query;
      const Users = getMongoRepository(User);
      const Usuario = await Users.findOne(String(id));
      if (Usuario) {
        ctx.status = 400;
      }
      ctx.body = Usuario.Carrito;
    });

    app
      .use(router.routes())
      .use(router.allowedMethods())
      .listen(process.env.DB_PORT, () =>
        console.log("Listening on port: ", process.env.DB_PORT)
      );
  })
    .catch((error: any) => console.log(error))
    ;

/* NOTA: Trabajando con TypeORM y Typescript probablemente la mejor página del mundo sea: https://typeorm.delightful.studio/index.html
   Ejemplo (https://typeorm.delightful.studio/classes/_repository_mongorepository_.mongorepository.html#findoneandreplace):
   
   findOneAndReplace

   findOneAndReplace(query: ObjectLiteral, replacement: Object, options?: FindOneAndReplaceOption): Promise<FindAndModifyWriteOpResultObject>

   Defined in repository/MongoRepository.ts:208

   Find a document and replace it in one atomic operation, requires a write lock for the duration of the operation.
   Parameters
   query: ObjectLiteral
   replacement: Object
   Optional options: FindOneAndReplaceOption
   Returns Promise<FindAndModifyWriteOpResultObject> (https://typeorm.delightful.studio/interfaces/_driver_mongodb_typings_.findandmodifywriteopresultobject.html)

   WOW, AHORA SE QUE RETORNA LA FUNCIÓN, CERO TIEMPO DESPERDICIADO.
*/
