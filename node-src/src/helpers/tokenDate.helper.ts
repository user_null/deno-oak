export const addTime = (x: number | undefined): number =>
  Math.floor(x === undefined || !x ? Date.now() / 1000 : Date.now() / 1000 + x);
/* 
   Un valor numérico JSON que representa el número de segundos desde 1970-01-01T00: 00: 00Z UTC hasta la fecha / hora UTC especificada, ignorando los segundos intercalares. Esto es equivalente a la definición de IEEE Std 1003.1, 2013 Edition [POSIX.1] "Segundos desde Epoch", en la que cada día se contabiliza exactamente por 86400 segundos, además de que se pueden representar valores no enteros. Consulte RFC 3339 [RFC3339] para obtener detalles sobre la fecha / hora en general y UTC en particular.
 */
