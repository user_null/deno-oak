import "reflect-metadata";
import * as Koa from "koa";
import * as jwt from "jsonwebtoken";
import * as koaBody from "koa-body";
export const authenticateToken = async (ctx: Koa.Context, next: Koa.Next) => {
  const body =
    typeof ctx.request.body === "string"
      ? JSON.parse(ctx.request.body)
      : ctx.request.body;
  const token = ctx.request.header.token || body.token;
  if (!(ctx.request.method === "GET")) {
    jwt.verify(token, "SECRET", (error: unknown, decoded: unknown) => {
      error && ctx.throw(403);
      ctx.request.body = decoded;
    });
  }
  await next();
};
