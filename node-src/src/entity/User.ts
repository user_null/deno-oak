import "reflect-metadata";
import { Entity, ObjectIdColumn, ObjectID, Column } from "typeorm";
import { Product } from "./Product";

@Entity()
export class User {
  @ObjectIdColumn()
  id: ObjectID;

  @Column()
  Nombre: string;

  @Column()
  Telefono: string;

  @Column()
  Email: string;

  @Column()
  Password: string;

  @Column()
  Admin: boolean;

  @Column()
  Direccion: ObjetoDireccion;

  @Column()
  Carrito: Array<string | Product>;
}

type ObjetoDireccion = {
  Calle: string;
  Altura: string;
  CodigoPostal: string;
  Piso?: string;
  Departamento?: string;
};
