import { Entity, ObjectIdColumn, ObjectID, Column } from "typeorm";
type CatList = Array<string>;
type PicList = Array<string>;

@Entity()
export class Product {
  @ObjectIdColumn()
  id: ObjectID;
  @Column()
  Nombre: string;
  @Column()
  Descripcion: string;
  @Column()
  Categoria: CatList;
  @Column()
  Precio: string;
  @Column()
  StockDisponible: string;
  @Column()
  Fotos: PicList;
}
