# Proyecto Backend con Node y Deno

## Instrucciones

Esta todo organizado para correlo con Docker
`$ docker-compose up`

Si no queres correrlo asi tenes que abrir tres terminales y correr todo por separado

## Organizacion

La instancia de deno/oak contiene toda la validacion y hace todas las request al controlador de Mongo mediante tokens firmados,
estos tokens son verificados en el controlador de Mongo y de ser válidos se realiza la transacción.

Por qué esto?

El controlador de Mongo para Deno está, al igual que Deno, super verde. Resultaba casi imposible conectar deno a Mongo Atlas y todos los ejemplos on-line utilizaban bases de datos locales, lo cual no es ideal en mi caso.

La solucion fue agarrar el stack node/koa y hacer todas conexiones a la base de datos usando de base typeORM, funciona perfecto, todo con typescript.
Pero surge un problema, deberia asegurarme que las operaciones son validas, ahí es donde entra web token, solamente necesito comprobar la firma y lo doy por válido.

De este desastre surgió un beneficio inesperado por mi. Al ser dos programas separados, las conexiones a la base de datos están protegidas de ataques directos (ya que con NGINX podrias simplemente no mostrar la ruta) y aun mejor, se pueden destinar correctamente recursos entre el "Front-end" de la base de Datos y el controlador de la base de datos, por si otras app de cualquier naturaleza quisieran usar sus end-point.

Ahora, es conveniente hacer todo esto utilizando deno/oak y node/koa? No, para nada, Deno está verde, Oak está verde, Koa en relación a los anteriores está MUCHO más maduro pero no es ideal (aunque me encante).

Typescript es genial, pero las soluciones y/o ejemplos no tienden a escribir los _types_ asi que toca leerse archivos larguísimos, adivinar o rezar que el IDE entienda.
(Incluso todas las anteriores).

Los websockets están por separado porque me pintaba. Además ni siquiera usa Socket.io en el backend. Lo más plano posible. Imaginate que mando HTML asi crudo.

### Resumen

```
deno => (token seguro) => node/Mongo
websocket => (token seguro) => node/Mongo
```

## Variables de Entorno

Hay un archivo llamado `.env.example` con este contenido

- URI= URL conexion Mongo Atlas
- PORT= Puerto donde corre la cosa esa de deno, default 8080, si no está definido es 8081
- DB_PORT= Puerto donde corre la cosa esa de node, default 3000
- SOCKET= El websocket, default 8082
- JWT_TOKEN_SECRET= POR FAVOR PONER UN SECRETO QUE SEA LARGO ;) default: SECRET
- JWT_ACCESS_TOKEN_EXP=100000 Este ni lo uso
- JWT_REFRESH_TOKEN_EXP=10000

## FAQS

Capo vos entendes todo lo que escribiste?

- Si

Seguro?

- No, bueno si.

Por qué no usaste express/node, Nest.js o XYZ como una persona normal?

- Y la gracia de eso? Ya voy a tener que escribir código para una empresa con tecnologias aburridas (muy válidas y testeadas, muy importantes)

Tan dificil es escribir un if?

- Me pinta usar operadores ternarios mientras sea mi código. Yo los entiendo perfecto. Claro que si es molestia para otro usaria if-else.

Buscas trabajo?

- SI, mi correo (el que miro) dumpnull1006@protonmail.ch, (el que parece más serio) agustinrios1006@gmail.com .
